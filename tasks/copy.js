'use strict';
import path from 'path';
import {distPath, copyFiles, serverFiles} from './config';

export default function Copy (gulp) {
  return gulp.task('copy', () => {
     gulp.src(serverFiles)
      .pipe(gulp.dest(path.join(distPath, 'server')))
     gulp.src(copyFiles)
      .pipe(gulp.dest(distPath));
  });
}