import path from 'path';

export const distPath = path.join(process.cwd(), './dist');
export const copyFiles = [
  path.join(process.cwd(), '.babelrc'),
  path.join(process.cwd(), 'index.js'),
  path.join(process.cwd(), 'package.json'),
];
export const serverFiles = [
  path.join(process.cwd(), 'server/**')
];
