// generated on 2017-05-13 using generator-webapp 2.3.2
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync').create();
const del = require('del');
const wiredep = require('wiredep').stream;
const runSequence = require('run-sequence');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

var dev = true;
let APP_PATH = 'client/';

gulp.task('styles', () => {
  return gulp.src(APP_PATH + 'app/styles/*.scss')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(APP_PATH + '.tmp/styles'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
  return gulp.src(APP_PATH + 'app/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(APP_PATH + '.tmp/scripts'))
    .pipe(reload({stream: true}));
});

function lint(files, options) {
  return gulp.src(files)
    .pipe($.eslint({ fix: true }))
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint(APP_PATH + 'app/scripts/**/*.js')
    .pipe(gulp.dest(APP_PATH + 'app/scripts'));
});
gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js')
    .pipe(gulp.dest('test/spec'));
});

gulp.task('html', ['styles', 'scripts'], () => {
  return gulp.src(APP_PATH + 'app/*.html')
    .pipe($.useref({searchPath: [APP_PATH + '.tmp', APP_PATH + 'app', APP_PATH + '.']}))
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.cssnano({safe: true, autoprefixer: false})))
    .pipe($.if('*.html', $.htmlmin({collapseWhitespace: true})))
    .pipe(gulp.dest('client/dist'));
});

gulp.task('images', () => {
  return gulp.src(APP_PATH + 'app/images/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('client/dist/images'));
});

gulp.task('fonts', () => {
  return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
    .concat('client/app/fonts/*'))
    .pipe($.if(dev, gulp.dest(APP_PATH + '.tmp/fonts'), gulp.dest('client/dist/fonts')));
});

gulp.task('extras', () => {
  return gulp.src([
    APP_PATH + 'app/*',
    'APP_PATH + !app/*.html'
  ], {
    dot: true
  }).pipe(gulp.dest('client/dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'client/dist']));

gulp.task('serve', () => {
  runSequence(['clean', 'wiredep'], ['styles', 'scripts', 'fonts'], () => {
    browserSync.init({
      notify: false,
      port: 9999,
      server: {
        baseDir: [APP_PATH + '.tmp', APP_PATH + 'app'],
        routes: {
          '/bower_components': APP_PATH + 'bower_components'
        }
      }
    });

    gulp.watch([
      APP_PATH + 'app/*.html',
      APP_PATH + 'app/images/**/*',
      APP_PATH + '.tmp/fonts/**/*'
    ]).on('change', reload);

    gulp.watch(APP_PATH + 'app/styles/**/*.scss', ['styles']);
    gulp.watch(APP_PATH + 'app/scripts/**/*.js', ['scripts']);
    gulp.watch(APP_PATH + 'app/fonts/**/*', ['fonts']);
    gulp.watch('bower.json', ['wiredep', 'fonts']);
  });
});

gulp.task('serve:dist', ['default'], () => {
  browserSync.init({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['client/dist']
    }
  });
});

gulp.task('serve:test', ['scripts'], () => {
  browserSync.init({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': APP_PATH + '.tmp/scripts',
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch(APP_PATH + 'app/scripts/**/*.js', ['scripts']);
  gulp.watch(['test/spec/**/*.js', 'test/index.html']).on('change', reload);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
});

// inject bower components
gulp.task('wiredep', () => {
  gulp.src(APP_PATH + 'app/styles/*.scss')
    .pipe($.filter(file => file.stat && file.stat.size))
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest(APP_PATH + 'app/styles'));

  gulp.src(APP_PATH + 'app/*.html')
    .pipe(wiredep({
      exclude: ['bootstrap-sass'],
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('client/app'));
});

gulp.task('build', ['lint', 'html', 'images', 'extras', 'fonts'], () => {
  return gulp.src('client/dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', () => {
  return new Promise(resolve => {
    dev = false;
    runSequence(['clean', 'wiredep'], 'build', resolve);
  });
});
