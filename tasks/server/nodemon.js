'use strict';
import nodemon         from 'gulp-nodemon';

export default function Nodemon (gulp) {

  return gulp.task('nodemon', () => {

    const env = process.env.NODE_ENV = process.env.NODE_ENV || 'local';
    
    let nodemonOptions = {
      script: './index.js',
      // exec: './node_modules/babel',
      ext: 'js',
      env: { 'NODE_ENV': env },
      verbose: true,
      ignore: ['node_modules/'],
      watch: ['server/*', 'index.js']
    };

    let stream = nodemon(nodemonOptions);
    

    stream
      .on('restart', () => console.log('restarted!'))
      .on('crash', function(e) {
        console.error('Application has crashed!\n', e)
        stream.emit('restart', 10)  // restart the server in 10 seconds
      });
  });
}
