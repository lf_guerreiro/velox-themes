'use strict';

import jshint from 'gulp-jshint';

export default function JsTest (gulp) {
   return gulp.task('jshint', () => {
      return gulp.src('server/**/*.js')
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('jshint-stylish', {beep: true}))
    });
}
