'use strict';
import gulp from 'gulp';
import Nodemon from './nodemon.js';
import JsTest from './jstest.js';
import Copy from '../copy';
import Clean from '../clean';

Copy(gulp);
Nodemon(gulp);
JsTest(gulp);
Clean(gulp);

gulp.task('build', ['clean', 'copy']);


gulp.task('watch', () => {
  gulp.watch('server/**/*.js', ['jshint']);
})

gulp.task('node', ['nodemon', 'watch']);
