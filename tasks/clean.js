'use strict';
import del from 'del';
import {distPath} from './config';

export default function Clean (gulp) {
  return gulp.task('clean', ['copy'], () => {
    return del.sync([`${distPath}/**`], {force: true});
  });
}