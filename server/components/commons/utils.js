'use strict';
import fs from "fs";
import path from "path";

export function isJSON (str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

export function successHandler (req, res, body) {
  res.status(200);
  if (isJSON(body)) {
    res.json(JSON.parse(body));
  } else {
    res.send(body);
  }
}

export function sendIndex(req, res) {
  const _root = path.join(__dirname,'../../../../');
  const _env = process.env.NODE_ENV;
  // const _folder = _env === "production" ? "dist" : "dev";
  const _folder = 'app';

  res.type(".html");

  fs.createReadStream(path.join(`${_root}client/${_folder}/index.html`)).pipe(res);
}
