'use strict';

import {isJSON} from 'components/commons/utils';

export function pageNotFound(req, res) {
  const viewFilePath = '404';
  const statusCode = 404;
  const result = {
    status: statusCode
  };

  res.status(result.status);

  res.render(viewFilePath, (err) => {
    if (err) { return res.json(result, result.status); }
    res.render(viewFilePath);
  });
}

export function errorHandler(req, res, error) {
  let status = error.statusCode || 500;
  res.status(status).send(error)
}