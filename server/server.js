'use strict';

import os from 'os';
import express from 'express';
import http from 'http';
import AppConfig from 'config/app.conf';
import Routes from 'routes/index';

const PORT = process.env.PORT || 3333;

const app = express();
AppConfig.init(app);
Routes.init(app);

http.createServer(app)
  .listen(PORT, () => {
    console.log(`up and running @: ${os.hostname()} on port: ${PORT}`);
  });
