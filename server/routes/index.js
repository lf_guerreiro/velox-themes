import { pageNotFound } from '../components/errors';
import ThemeRoutes from '../api/theme/routes/theme.routes';
import {sendIndex} from '../components/commons/utils';
import path from 'path';

export default class Routes {
  static init(app) {

    const _root = path.join(__dirname,'../..');
    const _env = process.env.NODE_ENV;
    const _folder = _env === 'production' ? `${_root}/client/dist` : `${_root}/client/app`;
    app.use('/api/themes', ThemeRoutes);

    app.route('/:url(api|auth|components|app|bower_components|assets)/*')
      .get(pageNotFound);

    app.route('/*')
      .get((req, res) => {
        res.sendFile(path.join(`${_folder}/index.html`));
      });
  }
}
