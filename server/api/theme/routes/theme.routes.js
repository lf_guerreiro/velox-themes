'use strict';

import {ThemeController} from '../controller/theme.controller';
import express from 'express';

const router = express.Router();
const Theme = new ThemeController();

router.get('/', Theme.getAll);
router.post('/', Theme.getAll);


module.exports = router;