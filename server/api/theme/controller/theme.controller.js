'use strict';
import sass from 'node-sass';
import _ from 'lodash';
import fs from 'fs';
import path from 'path';

//api/themes?name=movico&color1=%23f1f1f1&color2=%23f2f2f2&color3=%23f3f3f3&color4=%23f4f4f4&color5=%23f5f5f5&color6=%23f6f6f6&color7=%23f7f7f7

const ERRORS = {
  badRequest: {
    status: 406,
    message: 'Parametros invalidos'
  },
  compileCss: {
    status: 500,
    message: 'Falha ao compilar o CSS'
  }
};

export class ThemeController {
  getAll(req, res) {

    console.log('chegou')
    let params = (req.method === 'GET') ? req.query : req.body;
    
    if ( _.isEmpty(params) ) {
      res.status(ERRORS.badRequest.status).send(ERRORS.badRequest);
      return;
    }

    const typeSend = (_.has(params, 'type')) ? params.type : 'download';
    const nameFile = (_.has(params, 'name') && !_.isEmpty(params.name)) ? params.name : 'parceiro';

    
    delete params.name;
    delete params.type;
    
    var configVariables =  JSON.stringify(params).replace(/,/g, ';').replace(/[\{\}\"]/gi, '');
    configVariables += ';';

    fs.readFile(path.join(process.cwd(), 'server/scss/velox.scss'), function(err, data) {
        sass.render({
            data: configVariables + data,
            outputStyle: 'compressed',
            includePaths: [
              path.join(process.cwd(), 'node_modules'),
              path.join(process.cwd(), 'server/scss/')
            ]
        }, function(err, result) {

          if (err) {
            ERRORS.compileCss.detail = err;
            res.status(ERRORS.compileCss.status).send(ERRORS.compileCss);
          }
          

          if ( typeSend === 'download' ) {
            let stream = fs.createWriteStream(`./${nameFile}.css`);
            stream.write(result.css.toString(), () => {
              res.download(`./${nameFile}.css`, (err) => {
                if (err) {
                  fs.unlink(`./${nameFile}.css`);
                }
                fs.unlink(`./${nameFile}.css`);
              });
            });
          } else {
            res.status(200).send(result.css.toString());
          }
        });
    });


  }
}

