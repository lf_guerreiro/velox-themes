'use strict';

import morgan from 'morgan';
import methodOverride from 'method-override';
import bodyParser from 'body-parser';
import contentLength from 'express-content-length-validator';
import helmet from 'helmet';
import express from "express";
import compression from 'compression';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import zlib from 'zlib';
import path from 'path';

export default class AppConfig {
  
  static init(application) {
    const _env = process.env.NODE_ENV;
    let _root = process.cwd();
    let _rootClient = path.join(_root, 'client/');
    
    let _views = '/server/views/';
    
    application.engine('html', require('ejs').renderFile);
    application.set('view engine', 'html');
    application.set('views', _root + _views);


    if ( _env === 'production' ) {
      application.use('/fonts', express.static(path.join(_rootClient + 'dist/fonts/')));
      application.use('/styles', express.static(path.join(_rootClient + 'dist/styles/')));
      application.use('/scripts', express.static(path.join(_rootClient + 'dist/scripts/')));
      application.use('/', express.static(_rootClient));
    } else {
      application.use('/styles', express.static(path.join(_rootClient + '/.tmp/styles/')));
      application.use('/scripts', express.static(path.join(_rootClient + '/.tmp/scripts/')));
      application.use('/', express.static(_rootClient));

      application.use(cors({
        'origin': '*',
        'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
        'preflightContinue': false,
        'optionsSuccessStatus': 204,
        'credentials': true
        })
      );
      application.use(morgan('dev'));
    }

    application.use(compression({
      level: zlib.Z_BEST_COMPRESSION,
      threshold: '1kb'
    }));

    application.set('x-powered-by', false);
    application.disable('etag');

   

    application.use(methodOverride());
    application.use(cookieParser());
    application.use(bodyParser.urlencoded({limit: '100mb', extended: false, parameterLimit: 100000 }));
    application.use(bodyParser.json({limit: '100mb'}));
    application.use(contentLength.validateMax({ max: 999999 }));
    application.use(helmet({
      frameguard: false
    }));
    application.use(helmet.noCache());
    console.log('App confirgurado');
  }
}
