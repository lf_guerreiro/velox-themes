'use strict';

import _     from 'lodash';
process.env.NODE_ENV = process.env.NODE_ENV || 'index';

let all = {
  port: process.env.PORT || 8080,
};

module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {});
