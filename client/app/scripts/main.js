
(function($) {
  var clipboardDemos= new Clipboard('.copy');
  clipboardDemos.on('success', function(e){
    e.clearSelection();
    console.info('Action:',e.action);
    console.info('Text:',e.text);
    console.info('Trigger:',e.trigger);
  });

  clipboardDemos.on('error',function(e){
  });


  $('.loader').hide();
  $('.color').colorPicker({
    opacity: false
  });

  $(document).on('click', '.header', function (e) {
    e.preventDefault();
    var item = $(this).closest('.ticket-item');
    item.toggleClass('open');

    if (item.hasClass('open')) {
      item.find('.control strong').html('ocultar');
    } else {
      item.find('.control strong').html('detalhes');
    }
  });



  // Bind changes colors
  $('.color').on('blur', function(e) {
    var $data = $(this).data();
    var self = $(this);
    var $targe;

    Object.keys($data).forEach(function(key) {
      if (key !== 'colorMode') {
        
        if (key === 'stepline' ) {
          $('#css-heade').remove();
          $('<style id="css-heade">.steps ol li:not(:last-child):before {background:'  + self.val() + '}</style>').appendTo('head');
          return;
        } else if (key === 'body' ) {
          $targe = self.closest('body').find('.wrapper-demo ' + '[data-' + key + ']');
          $targe.css($targe.data(key), self.val());
          return;
        } else if (key !== null && key.match(/card/g) !== null) {
          $targe = self.closest('body').find('.wrapper-demo.card ' + '[data-' + key + ']');
          $targe.css($targe.data(key), self.val());
          return;
        
        } else {
          $targe = self.closest('.panel-body').find('.wrapper-demo ' + '[data-' + key + ']');
          $targe.css($targe.data(key), self.val());
        }

      }
    });
  });



  $('.choose input[type="radio"]').on('change', function (e) {
    e.preventDefault();
    if ( $(this).val() === 'string') {
      $('#name').closest('div').addClass('hide');
      $('#name').val('');
    } else {
      $('#name').closest('div').removeClass('hide');
    }
  });

  $('#theme').on('submit', function (e) {
    e.preventDefault();
    var variables = $(this).serializeFormJSON();
  
    console.log('Ruiz aqui esta o JSON', variables);

    $.ajax({
        url: 'http://127.0.0.1:3333/api/themes',
        method: 'POST',
        data: variables, 
        beforeSend: function () {
          $('.loader').show();
        },
        success: function (response, status, xhr) {
          if (variables.type === 'download') {
              var filename = '';
              var disposition = xhr.getResponseHeader('Content-Disposition');
              if (disposition && disposition.indexOf('attachment') !== -1) {
                  var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                  var matches = filenameRegex.exec(disposition);
                  if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
              }

              var type = xhr.getResponseHeader('Content-Type');
              var blob = new Blob([response], { type: type });

              if (typeof window.navigator.msSaveBlob !== 'undefined') {
                  window.navigator.msSaveBlob(blob, filename);
              } else {
                  var URL = window.URL || window.webkitURL;
                  var downloadUrl = URL.createObjectURL(blob);

                  if (filename) {
                      var a = document.createElement('a');
                      if (typeof a.download === 'undefined') {
                          window.location = downloadUrl;
                      } else {
                          a.href = downloadUrl;
                          a.download = filename;
                          document.body.appendChild(a);
                          a.click();
                      }
                  } else {
                      window.location = downloadUrl;
                  }
                  setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
              }
          } else {
            $('.loader').hide();
            $('.config').hide();
            $('.conclusion').removeClass('hide');
            $('.conclusion textarea').val(response);
          }
        },
        error: function (error) {
          console.log(error);
        }
    })
  });

  $(document).on('click', '.back', function (e) {
    e.preventDefault();
    $('.config').show();
    $('.conclusion').addClass('hide');
  });


})(jQuery);

(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);

